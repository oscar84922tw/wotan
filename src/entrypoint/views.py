from django.contrib import auth
from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from entrypoint.models import Feedback
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth import authenticate, login

import json

def forget_passwd(request):
    return render(request, 'forget_password.html')
    
def my_login(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/index/')

    username = request.POST.get('username', '')
    password = request.POST.get('password', '')
    user = auth.authenticate(username=username, password=password)

    if user is not None and user.is_active:
        auth.login(request, user)
        return HttpResponseRedirect('/')
    else:
        return render(request, 'terms.html')

def index(request):
    return render(request, 'index.html')



def contact_us(request):
    return render(request, 'contact_us.html')


def term(request):
    return render(request, 'terms.html')


def send_feedback(request):
    name = request.POST['user_name']
    email = request.POST['email']
    feedback = request.POST['message']
    f = Feedback.objects.create(name=name, email=email, contain=feedback)
    return render(request, 'contact_us.html')


def register(request):
    if request.method == 'POST':
        new_user = User.objects.create(username=request.POST['username'],
                                       password=request.POST['password'],
                                       email=request.POST['email'])
        login(request,new_user)
        return HttpResponse(json.dumps({'signal':'sucuess'}), content_type='application/json')
    return HttpResponseRedirect('/')


def page_not_found(request):
    return render(request, '404.html')
